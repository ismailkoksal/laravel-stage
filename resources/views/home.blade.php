@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Address') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Number') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($companies as $company)
                                <tr>
                                    @if($company->user_id == $user)
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->address }}, {{ $company->zipcode }}, {{ $company->city }}</td>
                                        <td>{{ $company->email }}</td>
                                        <td>{{ $company->number }}</td>
                                    @endif
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
