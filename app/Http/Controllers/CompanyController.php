<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('add');
    }

    public function add(Request $request)
    {
        $user_id = Auth::id();
        $name = $request->input('name');
        $address = $request->input('address');
        $zipcode = $request->input('zipcode');
        $city = $request->input('city');
        $email = $request->input('email');
        $number = $request->input('number');

        if($name && $address && $zipcode && $city && $email && $number)
        {
            $company = new Company();
            $company->user_id = $user_id;
            $company->name = $name;
            $company->address = $address;
            $company->zipcode = $zipcode;
            $company->city = $city;
            $company->email = $email;
            $company->number = $number;
            $company->save();
            return redirect()->route('home');
        }
    }
}
